from flask import Blueprint
from ..models import User, Role, Like
from ..extensions import db, bcrypt
from flask import request, abort, make_response
from ..auth import access_token_required

users = Blueprint('users', __name__)

@users.get('/')
def get_users():
    users = db.session.execute(db.select(User)).scalars()
    return [x.toDict() for x in users]

@users.post('/')
def new_user():
    request_body = request.get_json()
    username = request_body['username']
    email = request_body['email']
    password = request_body['password']
    password_hash = bcrypt.generate_password_hash(password)
    # Obtener el id del rol llamado "user"
    role_id = db.session.execute( db.select(Role.id).where(Role.name=='user') ).one()[0]
    user = User(username=username, email=email, password=password_hash, role_id=role_id)
    db.session.add(user)
    db.session.commit()
    return {'success': 'User created successfully'}

@users.get('/<user_id>')
def get_user(user_id):
    stm = db.select(User).where(User.id==user_id)
    user = db.session.execute(stm).scalar()
    if user is not None:
        return user.toDict()
    else:
        return make_response({'error': 'user not found'}, 404)

@users.route('/<int:user_id>', methods=['PATCH', 'PUT'])
def put_user(user_id):
    # mala idea: usa los datos de entrada como si estuviesen bien
    request_body = request.get_json()
    # / mala idea
    user = db.session.get(User, user_id)
    if user is not None:
        user.update_from_dict(request_body)
        db.session.add(user)
        db.session.commit()
        return {'success': 'User updated successfully'}
    else:
        return make_response({'error': 'user not found'}, 404)

@users.delete('/<int:user_id>')
@access_token_required
def delete_user(token_values, user_id):
    whoami = token_values['user_id']
    # Faltaría comprobar que el usuario sigue existiendo
    # Porque si no .one() nos puede lanzar una excepción
    role = db.session.execute(db.select(Role.name).join(User).where(User.id==whoami)).one()[0]

    if not (whoami == user_id or role == 'admin'):
        return make_response({'error': 'Unauthorized'}, 401)
    stm = db.delete(User).where(User.id==user_id)
    result = db.session.execute(stm)
    db.session.commit()
    return {'success': 'User deleted successfully'}


@users.get('/<user_id>/likes')
@access_token_required
def likes_user(user_id):
    likes = Like.query.filter_by(author_id=user_id).all()
    return {'likes': [like.toDict() for like in likes]}


@users.post('/<int:user_id>/follow')
@access_token_required
def follow_user(token_values, user_id):
    whoami = token_values['user_id']
    me = db.session.execute (db.select(User).where(User.id==whoami)).scalar()
    following = db.session.execute (db.select(User).where(User.id==user_id)).scalar()
    me.follows.append(following)
    db.session.commit()
    return {'success': 'User followed successfully'}


@users.post('/<int:user_id>/follow')
@access_token_required
def unfollow_user(token_values, user_id):
    whoami = token_values['user_id']
    me = db.session.execute (db.select(User).where(User.id==whoami)).scalar()
    following = db.session.execute (db.select(User).where(User.id==user_id)).scalar()
    me.follows.remove(following)
    db.session.commit()
    return {'success': 'User followed successfully'}


@users.get('/<user_id>/followers')
@access_token_required
def followers_user(user_id):
    me = db.session.execute (db.select(User).where(User.id==whoami)).scalar()
    followers = me.followers
return {'followers': [Follow.toDict() for follower in followers]}


@users.get('/<user_id>/followers')
@access_token_required
def following_user(user_id):
    me = db.session.execute (db.select(User).where(User.id==whoami)).scalar()
    following = me.following
return {'following': [Follow.toDict() for following in followings]}

