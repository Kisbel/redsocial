from flask import Blueprint, request
from ..models import Tweet, Like
from ..extensions import db
from ..auth import access_token_required

tweets = Blueprint('tweets', __name__)

@tweets.get('/')
def get_tweets():
    author_filter = request.args.get('author_id')
    query = db.select(Tweet)
    if author_filter:
        query = query.where(Tweet.author_id == author_filter)
    tweets = db.session.execute(query).scalars()
    return [x.toDict() for x in tweets]

@tweets.post('/')
@access_token_required
def post_tweet(token_values):
    request_body = request.get_json()
    body = request_body['body']
    author_id = token_values['user_id']
    tweet = Tweet(author_id=author_id, body=body)
    db.session.add(tweet)
    db.session.commit()
    return {'success': 'Tweet created successfully'}

@tweets.delete('/<int:id>')
def delete_tweet(tweet_id):
    tweet_id = Tweet(id=id)
    tweet = Tweet.query.get(tweet_id)
    if tweet is None:
        return make_response(404, {'error': 'Tweet not found'})
    db.session.delete (tweet)
    db.session.commit()
    return make_response(tweet.toDict, 201, {'success': 'Tweet deleted successfully!'})

@tweets.post('/<int:tweet_id>/like')
@access_token_required
def like_tweet(token_values, tweet_id):
    author=db.session.execute (db.select(User).where(User_id == author_id)).scalar()
    tweet=db.session.execute (db.select(User).where(Tweet_id == tweet_id)).scalar()
    author.likes.append(tweet)
    db.session.commit()
    return {'success': 'Tweet liked successfully'}


@tweets.post('/<int:tweet_id>/like')
@access_token_required
def unlike_tweet(token_values, tweet_id):
    author=db.session.execute (db.select(User).where(User_id == author_id)).scalar()
    tweet=db.session.execute (db.select(User).where(Tweet_id == tweet_id)).scalar()
    author.likes.remove(tweet)
    db.session.commit()
    return {'success': 'Tweet liked successfully'}

@tweets.post('/tl')
@access_token_required
def timeline_tweets(token_values, tweet_id):
    current_user = token_values ["user_id"]
    user=db.session.execute (db.select(User).where(User.id == current_user)).scalar()
    for following in user.follows:
            tweets = tweets + following.tweets
    tweets = sorted (tweets, )

    return {'timeline': [tweets.toDict() for like in likes]}


@tweets.post('/tl')
@access_token_required
def timeline_tweets(token_values, tweet_id):
    current_user = token_values["user_id"]
    user = User.query.get(current_user)
    tweets = db.session.query(Tweet, User).join(User).\
        filter(User.id.in_([u.id for u in current_user.follows])).\
        order_by(Tweet.created_at.desc()).all()

    # Return the timeline as a list of dictionaries
    timeline = [{'id': t.id, 'text': t.text, 'created_at': t.created_at, 'user': u.toDict()} for t, u in tweets]
    return jsonify({'timeline': timeline})




