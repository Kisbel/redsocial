from flask import Blueprint
from ..models import User
from ..extensions import db, bcrypt
from flask import request, abort, make_response
from ..auth import refresh_token_required, jwt_refresh_token, jwt_access_token

auth = Blueprint('auth', __name__)

@auth.post('/login')
def authenticate_user():
    request_body = request.get_json()
    try:
        username = request_body['username']
        password = request_body['password']
    except:
        return make_response(400, {'error': 'Bad request'})

    stm = db.select(User).where(User.username==username)
    user = db.session.execute(stm).scalar()
    if bcrypt.check_password_hash(user.password, password):
        return {
            'access_token': jwt_access_token(user_id=user.id),
            'refresh_token': jwt_refresh_token(user_id=user.id)
        }
    else:
        return make_response({'error': 'Incorrect credentials'}, 401)

@auth.post('/refresh')
@refresh_token_required
def refresh_token(token_data):
    user_id = token_data['user_id']
    return { 'access_token': jwt_access_token(user_id=user_id) }

