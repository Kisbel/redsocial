from functools import wraps
import jwt
import os
from flask import request, jsonify
from datetime import datetime, timedelta

token_secret = os.getenv("FLASK_SECRET")

def jwt_access_token(user_id=None):
    expiration_time = datetime.now()+timedelta(minutes=10)
    return jwt_encode({"user_id": user_id}, expiration_time)

def jwt_refresh_token(user_id=None):
    expiration_time = datetime.now()+timedelta(days=1)
    return jwt_encode({"user_id": user_id}, expiration_time)

def jwt_encode(payload, expiration_time):
    return jwt.encode({**payload, "exp": expiration_time}, token_secret,
        algorithm="HS256")

def jwt_decode(token):
    return jwt.decode(token, token_secret, algorithms=["HS256"])

def token_required(header):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            token = None
            if header in request.headers:
                token = request.headers[header]

            if not token:
                return jsonify({'message': 'a valid token is missing'})
            try:
                data = jwt_decode(token)
            except:
                return jsonify({'message': 'token is invalid'})

            return f(data, *args, **kwargs)
        return decorated_function
    return decorator

access_token_required = token_required("x-access-token")
refresh_token_required = token_required("x-refresh-token")
