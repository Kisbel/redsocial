from .extensions import db

class User(db.Model):
     __tablename__= 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String, nullable=False)
    password = db.Column(db.String, nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'), nullable=True)
    role = db.relationship('Role', foreign_keys='User.role_id')
    tweet = db.relationship ('Tweet', secondary = 'like', back_populates='liked_by')
    likes = db.relationship ('Tweet', secondary = 'like', back_populates='liked_by')
    follows = db.relationship ('Follow', secondary='follow', back_populates='users')
        def toDict(self):
        # Password no lo retornamos nunca, así que lo mantenemos fuera de la función toDict
        return {'id': self.id, 'username': self.username, 'email': self.email}
    def update_from_dict(self, d):
        for k, v in d.items():
            setattr(self, k, v)

class Role(db.Model):
     __tablename__= 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)

class Tweet(db.Model):
    __tablename__= 'tweet'
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String, nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    date = db.Column (db.DateTime, nullable=False)
    author = db.relationship('User', foreign_keys='Tweet.author_id')
    liked_by= db.relationship ('User', secondary = 'like', back_populates='likes')
    def toDict(self):
        return {'id': self.id, 'author_id': self.author_id, 'body': self.body}

class Like(db.Model):
     __tablename__= 'likes'
    tweet_id = db.Column(db.Integer, db.ForeignKey('tweet.id'), primary_key= True, nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key= True, nullable=False)

class Follow(db.Model):
    __tablename__= 'follow'
    followers = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key= True, nullable=False)
    following = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key= True, nullable=False)
    users = db.relationship ('User', secondary='follow', back_populates='follows')




