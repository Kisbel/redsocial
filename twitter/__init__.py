from flask import Flask
from .routes.users import users
from .routes.tweets import tweets
from .routes.auth import auth
from .extensions import db, migrate, bcrypt

def create_app():
    # create the app
    app = Flask(__name__)
    # configure the SQLite database, relative to the app instance folder
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///project.db"

    # Initialize extensions
    db.init_app(app)
    migrate.init_app(app, db)
    bcrypt.init_app(app)


    @app.get("/")
        def serve_app():
        return render_template('app.html')


    app.register_blueprint(users, url_prefix="/users")
    app.register_blueprint(tweets, url_prefix="/tweets")
    app.register_blueprint(auth, url_prefix="/auth")

    return app
