import m from "mithril";
import {Input, EmailInput, PasswordInput} from "./form_components";

let state = {
  username: null,
  password: null,
  passwordConfirmation: null,
  email: null,

  usernameSetter(){
    return (x)=> this.username = x;
  },
  passwordSetter(){
    return (x)=> this.password = x;
  },
  passwordConfirmationSetter(){
    return (x)=> this.passwordConfirmation= x;
  },
  emailSetter(){
    return (x)=> this.email= x;
  },

  isValid(){
    // Podemos comprobar si los valores están bien
    return true;
  }
}


let Register = {
  sendForm: function(e){
    e.preventDefault();
    if(!state.isValid()){
      // Podemos tomar ciertas decisiones si el estado es inválido:
      // - Pintar rojo el formulario?
      return;
    }
    let data = {
      username: state.username,
      email: state.email,
      password: state.password,
      passwordconfirmation: state.passwordConfirmation
    }

    m.request({
      method: "POST",
      url: "/api/users",
      body: data
    }).then(()=>{
      // User created -> go to login screen
      m.route.set("/login");
    }).catch((err)=>{
      console.error(err);
    });

  },

  view: function(){
    return <form id="register" onsubmit={this.sendForm}>
        <Input update={state.usernameSetter()} label="Username: "/>
        <EmailInput update={state.emailSetter()}  label="Email: "/>
        <PasswordInput update={state.passwordSetter()} label="Password: "/>
        <PasswordInput update={state.passwordConfirmationSetter()} label="Repeat Password: "/>
        <button type="submit">Registrarse</button>
      </form>
  }
}

export {Register}



