import m from "mithril";

function Input (initialVnode){
      //ponemos atributo update con la funcion para llamarla cuando se cambia el imput
    function updateValue(e){
        initialVnode.attrs.update(e.target.value); //QUE ES ESTA LINEA???
    } 
    return {
    view: (vnode)=> <div> 
        <label>{initialVnode.attrs.label}</label> //Y QUE ES ESTA LINEA??
        <input oninput={updateValue}/> 
    </div>
    } //POR QUE NO TIENE TYPE
}

function PasswordInput(initialVnode){
    //atributo update
    function updateValue(e){
        initialVnode.attrs.update (e.target.value);
    }
    return {
        view: (vnode)=> <div>
            <label>{initialVnode.attrs.label}</label>
            <input oninput={updateValue} type="password"/>
        </div>
    }
}

function EmailInput(initialVnode){
    //update attribute
    function updateValue(e){
      initialVnode.attrs.update( e.target.value );
    }
    return {
      view: (vnode)=> <div>
          <label>{initialVnode.attrs.label}</label>
          <input oninput={updateValue} type="email" />
        </div>
    }
  }
 
export {Input, PasswordInput,EmailInput};
