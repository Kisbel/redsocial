import m from "mithril";
import {Register} from ".register";

let root = document.body;

let Register = {
    view: function (){
        return <form id = "register" onsubmit= {register}>
            <input type = "text"/>
            <input type = "email" />
            <input typee = "password"/>
            <input type = "password" />
            <button type = "subtmit"> Registrarse</button>
        </form>
    }
};

//PAGINA DE INICIO, ANTES PONIA SPLASHH
let Inicio = {
    view: function (){
        return <p>
            <a href="#!/login">Login</a><br/>
            <a href="#!/register">Register</a><br/>
            <a href="#!/tweets">Tweets</a><br/>
        </p>
    }
}

//aun no hemos hecho el login 
let Login = {
    view: function(){

    }
}

function Tweet (tweet) {
    return {
        view: function(){
            return <article> //meter en un article todo pa q lo devuelva supongo
            <section>{tweet.body}</section>
            <time datetime={tweet.date}>{tweet.date}</time> //iguala datetime con el campo date de tweet y lo obtiene fuera de los picos
            <span>{tweet.uthor_id}</span> //span simplemente es un contenedor para aplicar estilo al texto o agrupar elementos en línea.
            </article>
        }
    }
}

//OBJETO TWEETS, con lista tweets []. Obtiene y muestra los tweets de una API en la página web.
let Tweets = {
    tweets: [],
    oninit: function(){ //función oninit obtiene nuevos tweets de una API cada 10 segundos
        let updateTweets = ()=>{
            m.request({
                method: "GET",
                url: "/api/tweets"
            })
            .then((data)=>{
                this.tweets = data;
            });
        }
        updateTweets(); //updateTweets hizo ya una solicitud GET a la API de tweets y actualiza la lista de tweets en el objeto Tweets con los datos recibidos.
        setInterval(updatetweets, 10000); //temporizador que llama a updateTweets cada 10 segundos para actualizar la lista de tweets
    },                                   
    view: function(){ 
        return this.tweets.map(function(tweet){ // función map() para recorrer la lista de tweets almacenados en la propiedad tweets del objeto Tweets. 
//Para cada tweet en la lista, se crea un elemento Tweet utilizando el framework "Mithril" y la función m().
//La función m() se utiliza para crear elementos en la interfaz de usuario. En este caso, se está creando un elemento Tweet y se le está pasando el objeto tweet como argumento.
            return m(Tweet(tweet)); //devuelve una lista de elementos Tweet, representan cada tweet en la lista de tweets del objeto Tweets
        })
    }
}

//En resumen, esta función recorre la lista de tweets almacenados en el objeto Tweets y
//utiliza la función m() para crear un elemento Tweet para cada tweet en la lista. 
//Luego, se devuelve una lista de elementos Tweet que se pueden mostrar en la página web.

m.route(root, "/inicio", {
    "/inicio": Inicio,
    "/login": Login,
    "register": Register,
    "/tweets": Tweets,
})