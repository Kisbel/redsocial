import m from "mithrill";
import {TokenStorage} from "./token_storage";
import {Input, PasswordInput} from "./form_components";

let state = {
    username: null,
    passsword: null, 
    usernameSetter(){
        return(x)=> this.username = x;
    },
    passwordSetter(){
        return(x)=>this.password = x;
    }, isValid(){
        //comprobar que valores estan bien
    }
}

let Login = {

    sendform: function(e){
        e.preventDefault();
        let data = {
            username: state.username,
            password: state.password,
        }

        m.request({
            method: "POST",
            url: "api/auth/login",
            body: date
        }).then((tokens)=>{
            //usuario creado, ir a la página de login
            console.log("Tokens obtained succesfully");
            console.log(tokens);
            TokenStorage.setTokens(tokens);
            m.rout.set("/timeline");
        }).catch((Err)=>{
            console.error(err);
        });
    },

    view: function(){
        return <form id="login" onsubmit={this.sendForm}>
            <Input update={state.usernameSetter()} label="Username:"/>
            <PasswordInput update={state.passwordSetter()} label="Password:"/>
            <button type= "submit"> Login</button>
        </form>
    }
}

export {Login};