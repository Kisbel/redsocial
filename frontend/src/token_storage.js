let TokenStorage = {
    hasToken(){
        return null !== sessionStorage.getItem("access-token"); //DONDE HEMOS DEFINIDO SESSIONSTORAGE
    },
    getAccessToken(){
        return sessionStorage.getItem("access-token");
    },
    getRefreshToken(){
        return sessionStorage.getItem("refresh-token");
    },

    setAccessToken (access_token){
        sessionStorage.setItem("access-token", access_token);
    },
    setRefreshToken (refresh_token){
        sessionStorage.setItem("refresh-token", refresh_token);
    },
    setTokens({access_token, refresh_token}){
        if (access_token===undefined||refresh_token===undefined){
            throw "TokenStorage.setTokens needs a valid token";
        }
        this.setAccessToken(access_token);
        this.setRefreshToken(refresh_token);
    }
}

export {TokenStorage}